# gulagbot

A Discord bot that Gulags people.

## What Does It Do?

Whenever somebody says or does something stupid, like says a dumb pun or something, you invoke the bot like so:

```
!gulag @Person
```

With any number of people or roles supplied as arguments to the `!gulag` command. The bot will then keep track of how many times this person has been tagged for Gulagging. You should get a response somewhat like the following:

```
We have special place in Gulag for TestUser.
```

Periodically (every 20 seconds), the bot will poll all available voice channels and see if anybody is available to be Gulagged. If it does, it will:

* Search for a random voice channel with "Gulag" in the name. If this fails, stop.

* Move the user to the chosen voice channel

* Tell them a random joke from a library of quips

* Subtract one (1) from the Gulag Counter of the user in question

You can hot-subtract gulags from people by using `!praise`:

```
!praise @Person
```

## Usage

The bot registers totally-not-Discord-native commands for your use:

command|args|description
---|---|---
`!gulag`|Any number of @users|Adds 1 to the gulag counters of all users mentioned
`!praise`|Any number of @users|Subtracts 1 from the gulag counters of all users mentioned down to -3
`!checkgulag`|Any number of @users|Prints out the pending gulags for all users mentioned. If no users are mentioned, dumps all outstanding gulags

## Emotes

The bot also responds to emote reacts, and will emote react in tow depending on the results of running the equivalent command on the message in question:

emote|equivalent command|notes
---|---|---
`:gulag:`|`!gulag`|Will cause the bot to respond with `:stalinangry:` if it is available in the event that the gulag succeeds
`:praise:`|`!praise`|Will cause the bot to respond with `:stalinhappy:` if it is available in the event the praise succeeds

## Installation

The bot requires a running PostgreSQL server to store data in. Be sure you have one of those you can use.

Pull the Docker image from `rehashedsalt/gulagbot`. Set these environment variables and run it:

envvar|description|example
---|---|---
`DISCORD_TOKEN`|The token for your Discord bot|`NzkyNzE1NDU0MTk2MDg4ODQy.X-hvzA.Ovy4MCQywSkoMRRclStW4xAYK7I`
`GULAG_HUMILIATION`|The number of people that must be in a voice chat for the bot to gulag them|`3`
`GULAG_SCORE_MAX`|The highest number of racked-up gulags that any one person is allowed to have|`50`
`GULAG_SCORE_MIN`|The minimum score, and thus the highest amount of backed-up praise, any one person is allowed to have|`-3`
`PGHOST`|The hostname and port to your PostgreSQL database server|`localhost:5432`
`PGDATABASE`|The database to connect to|`gulagbot-db`
`PGUSER`|The username to connect to PostgreSQL as|`gulagbot-user`
`PGPASSWORD`|The password for the aforementioned PostgreSQL user|`Solarwinds123`

The environment variables are from the [pg](https://www.npmjs.com/package/pg) package and obey the rules of [libpq](https://www.postgresql.org/docs/9.1/libpq-envars.html), should you be interested in advanced usage.

## Permissions

This bot will need to be set up with a particular set of permissions:

`https://discord.com/api/oauth2/authorize?client_id=[MY CLIENT ID]&permissions=19991616&scope=bot%20applications.commands`

## Non-Docker Installation

Pull the repo down, provide the same environment variables as above, and run `node index.js`. Tested against Node 15.13.0, but earlier versions will probably work.

I'll officially support anything 14.0 and up. File a bug if something breaks.
