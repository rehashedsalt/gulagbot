/*
 * gulag.js
 * Core functions for the Gulag Bot
 * Copyright (C) 2021 Vintage Salt
 *
 * Distributed under terms of the MIT license.
 */

// Requires for this library
const pg = require('pg');

// This allows us to access exports easily
var gb = this;

// First step upon sourcing is to initialize the gulagbot, no questions asked
console.log('Connecting to database...');
const db = new pg.Client();
db.connect().then(() => {
	console.log('Database connection established');
});
// Then we ensure we have the DB objects we need
//
// This table stores information about users, including their name, last
// score modification date, and current score
db.query(`create table if not exists users(
		key		bigserial primary key,
		guildid		varchar(24) not null,
		id		varchar(24) not null,
		name		varchar(50) not null,
		score		float4 not null default 0,
		time		timestamptz default now(),
		stat_gulag	int not null default 0,
		stat_praise	int not null default 0,
		stat_max_gulag	float4 not null default 0,
		stat_min_gulag	float4 not null default 0,
		unique(guildid,id)
	)`);
db.query(`create index if not exists users_score_gt_0 on users(score)
		where score > 0
	`);
// This table stores a list of transactions. This ensures that users cannot
// exploit issues in the frontend to vote on a post more than once
db.query(`create table if not exists log(
		key		bigserial primary key,
		guildid		varchar(24) not null,
		plaintiffid	varchar(24) not null,
		defendantid	varchar(24) not null,
		messageid	varchar(24) not null,
		score		float4 not null,
		time		timestamptz default now(),
		note		text
	)`);
// This table stores a list of stupid things for the bot to send to people when
// they get moved to a gulag channel
// NOTE: THIS TABLE NEEDS THE pg_trgm EXTENSION
// ADD IT WITH: CREATE EXTENSION pg_trgm;
db.query(`create table if not exists quips(
		key		bigserial primary key,
		guildid		varchar(24) not null default 0,
		authorid	varchar(24) not null default 0,
		quip		text unique not null,
		time		timestamptz default now()
	)`);
// This table stores a list of stupid things for the bot to set peoples' names
// to
// NOTE: THIS TABLE NEEDS THE pg_trgm EXTENSION
// ADD IT WITH: CREATE EXTENSION pg_trgm;
db.query(`create table if not exists nicks(
		key		bigserial primary key,
		guildid		varchar(24) not null default 0,
		authorid	varchar(24) not null default 0,
		nick		text unique not null,
		time		timestamptz default now()
	)`);
// This is just a nicer looking view at the logs
db.query(`create or replace view view_log_pretty as
		select
			log.key			as key,
			log.guildid		as guildid,
			log.time		as time,
			plaintiffinfo.id	as plaintiff_id,
			plaintiffinfo.name	as plaintiff,
			defendantinfo.id	as defendant_id,
			defendantinfo.name	as defendant,
			log.score		as score,
			log.note		as note
		from log
		left join users plaintiffinfo on log.plaintiffid = plaintiffinfo.id
		left join users defendantinfo on log.defendantid = defendantinfo.id
		where log.score > 0.1
		or log.score < -0.1
		order by log.time asc
	`);

exports.config = {
	// What all commands need to be prefixed with
	COMMAND_PREFIX: "!",
	// Enable debugging features,
	DEBUG: false,
	// Time in milliseconds between attempts to enforce discipline on users
	ENFORCE_FREQUENCY: 5000,
	// The string that voice channels' lowercase variants must match to be
	// considered a candidate to move convicts to
	GULAG_MATCH_STRING: '.*gulag.*',
	// The number of users that must be in a voice chat before discipline is
	// allowed to be enacted. This prevents cheesing by sitting in a channel alone
	HUMILIATION: 3,
	// The ID of the gulagbot, must be set after requiring this file.
	ID: 0,
	// The maximum score a user is allowed to have
	SCORE_MAX: 50,
	// The minimum score a user is allowed to have
	SCORE_MIN: -3
}
exports.returnValues = {
	CAP: "The defendant has reached maximum or minimum score",
	DUPLICATE: "The plaintiff attempted to vote twice OR the quip is a duplicate",
	ERROR: "An uncaught exception prevented the transaction from completing",
	GUILTY: "The plaintiff has a score greater than 0",
	GULAGBOT: "The plaintiff attempted to modify the score of the gulagbot",
	NULL: "The plaintiff attempted to modify the defendant's score by 0",
	SAMEUSER: "The plaintiff and the defendant are the same user",
	SUCCESS: "The transaction was completed successfully"
}

// Attempts to have one user impact the score of another, with some constraints
// 	guildid		The ID of the guild in which the activity takes place
// 	plaintiffid	The ID of the user making the attempt
// 	defendantid	The ID of the user being affected
// 	defendantname	The name of the user being affected
// 	messageid	The ID of the message that caused the attempt
// 	score		The score to add to the defendant's score tally (accepts negatives)
// 	note		A note to add to the log entry (can be null)
// Returns:
// 	See returnValues above
exports.affectUser = async function(guildid, plaintiffid, defendantid, defendantname, messageid, score, note) {
	try {
		// Get some information
		plaintiffScore = await gb.getUserScore(guildid, plaintiffid);
		defendantScore = await gb.getUserScore(guildid, defendantid);
		duplicateTransactions = await db.query(`select key, time from log
			where guildid = $1
			and plaintiffid = $2
			and defendantid = $3
			and messageid = $4`, [
				guildid,
				plaintiffid,
				defendantid,
				messageid
			]);

		// Begin massive redflag check block
		if (defendantScore >= gb.config.SCORE_MAX && score > 0) {
			return gb.returnValues.CAP;
		} else if (defendantScore <= gb.config.SCORE_MIN && score < 0) {
			return gb.returnValues.CAP;
		} else if (defendantid != gb.config.ID && messageid != 0 && duplicateTransactions.rowCount > 0) {
			return gb.returnValues.DUPLICATE;
		} else if (plaintiffScore > 1) {
			return gb.returnValues.GUILTY;
		} else if (defendantid == gb.config.ID) {
			return gb.returnValues.GULAGBOT;
		} else if (score == 0) {
			return gb.returnValues.NULL;
		} else if (plaintiffid == defendantid) {
			return gb.returnValues.SAMEUSER;
		}

		// Calculate a new score and commit the transaction
		gb.logIfDebug('Old score: ' + defendantScore + ', adding ' + score);
		newScore = defendantScore + score;
		if (newScore > gb.config.SCORE_MAX) {
			newScore = gb.config.SCORE_MAX;
		} else if (newScore < gb.config.SCORE_MIN) {
			newScore = gb.config.SCORE_MIN;
		}
		gb.logIfDebug('New score: ' + newScore);
		db.query(`insert into users(
				guildid,
				id,
				name,
				score
			) values(
				$1,
				$2,
				$3,
				$4
			) on conflict (guildid,id) do update set
				score = $4,
				time = DEFAULT
			where users.guildid = $1 and users.id = $2`,
			[
				guildid,
				defendantid,
				defendantname,
				newScore
			]);
		gb.logTransaction(guildid, plaintiffid, defendantid, messageid, score, note);

		// Update the user's stats, if relevant
		try {
			// Get their stats
			// I use a blind select here as we would've just added the user to the DB
			// earlier in the transaction if they weren't already in it.
			gb.logIfDebug(guildid + ', ' + defendantid);
			defendantQuery = await db.query(`select
					stat_gulag,
					stat_praise,
					stat_max_gulag,
					stat_min_gulag
				from users
					where guildid = $1
					and id = $2
				`, [
					guildid,
					defendantid
				]);
			defendantData = defendantQuery.rows[0];
			// stat_gulag
			if (score > 0) {
				await db.query(`update users set stat_gulag = stat_gulag + 1 where id = $1 and guildid = $2`, [defendantid, guildid]);
			}
			// stat_praise
			else if (score < 0) {
				await db.query(`update users set stat_praise = stat_praise + 1 where id = $1 and guildid = $2`, [defendantid, guildid]);
			}
			// stat_max_gulag
			if (newScore > 0 && defendantData.stat_max_gulag < newScore) {
				await db.query(`update users set stat_max_gulag = $1 where id = $2 and guildid = $3`, [newScore, defendantid, guildid]);
			}
			// stat_min_gulag
			else if (newScore < 0 && defendantData.stat_min_gulag > newScore) {
				await db.query(`update users set stat_min_gulag = $1 where id = $2 and guildid = $3`, [newScore, defendantid, guildid]);
			}
		} catch(e) {
			// We silently-ish mask the failure here as the rest of the transaction went fine
			console.log("Error while updating stats");
			console.error(e);
		}

		return gb.returnValues.SUCCESS;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Notes a transaction down in the logs to prevent deduplication
// 	guildid		The ID of the guild in which the activity takes place
// 	plaintiffid	The ID of the user making the attempt
// 	defendantid	The ID of the user being affected
// 	messageid	The ID of the message that caused the attempt
// 	score		The score to add to the defendant's score tally (accepts negatives)
// 	note		A note to add to the entry (can be null)
// Returns:
// 	SUCCESS, ERROR
exports.logTransaction = async function(guildid, plaintiffid, defendantid, messageid, score, note) {
	try {
		db.query(`insert into log(
				guildid,
				plaintiffid,
				defendantid,
				messageid,
				score,
				note
			) values(
				$1,
				$2,
				$3,
				$4,
				$5,
				$6
			)`,
			[
				guildid,
				plaintiffid,
				defendantid,
				messageid,
				score,
				note
			]);
		return gb.returnValues.SUCCESS;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Gets the score for a particular user + guild combination
// 	guildid		The guildid of the user
// 	id		The ID of the user
// Returns:
// 	An integer
// 	ERROR
exports.getUserScore = async function(guildid, id) {
	try {
		rawScore = await db.query('select score from users where guildid = $1 and id = $2 limit 1', [guildid, id])
		if (rawScore.rowCount < 1) {
			return 0;
		} else {
			return rawScore.rows[0].score;
		}
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Gets the scoreboard for a particular guild, showing all users with
// positive score
// 	guildid		The guildid to pull statistics for
// Returns:
// 	The result of a PG query
// 	ERROR
exports.getScoreboard = async function(guildid) {
	try {
		return await db.query('select id, name, score from users where guildid = $1 and score > 0.5 order by score', [guildid]);
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Selects all of the log entries for a particular guild and parses them out
// into a nice, human-readable log.
// 	guildid		The guildid to pull logs for
// Returns:
// 	An array of strings in chronological order
// 	ERROR
exports.getPrettyLog = async function(guildid) {
	try {
		query = await db.query(`select * from view_log_pretty where guildid = $1`, [guildid]);
		returnrows = [];
		for (row of query.rows) {
			returnstring = ""
			key = row.key;
			time = row.time;
			plaintiffid = row.plaintiff_id;
			plaintiff = row.plaintiff || row.plaintiff_id;
			defendantid = row.defendant_id;
			defendant = row.defendant || row.defendant_id;
			score = row.score;
			note = row.note;
			// Handle the case where the plaintiff is the gulagbot
			if (plaintiffid == gb.config.ID) {
				plaintiff = "GULAGBOT";
			}
			// Add a timestamp
			returnstring += "[" + time.toUTCString() + "] ";
			// Figure out what went on based on actors and score
			if (score < 0 && plaintiffid == gb.config.ID) {
				// The bot disciplined them
				returnstring += "User " + defendant + " was disciplined: \"" + row.note + "\"";
			} else if (score > 0) {
				// Score was added to a user
				returnstring += "User " + plaintiff + " gulagged " + defendant + " by " + score;
			} else if (score < 0) {
				// Score was removed from a user
				returnstring += "User " + plaintiff + " praised " + defendant + " by " + (score * -1);
			} else {
				// Unknown/unhandled action
				returnstring += "Unknown action: Plaintiff: " + plaintiff + ", Defendant: " + defendant + ", Score: " + score + ", Note: " + note;
			}
			// Add our string to the array
			returnrows.push(returnstring);
		}
		// Return the array
		return returnrows;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Adds a quip to the gulagbot's repertoire
// 	guildid		The ID of the guild in which the quip was added
// 	authorid	The ID of the user who wrote the quip
// 	quip		The quip, verbatim. Accepts markdown (because Discord does)
// Returns:
// 	DUPLICATE, ERROR, SUCCESS
exports.addQuip = async function(guildid, authorid, quip) {
	try {
		similarEntries = await db.query('select key from quips where guildid = $1 and similarity(quip,$2) > $3', [guildid, quip, 0.75]);

		// Check for redflags
		if (similarEntries.rowCount > 0) {
			return gb.returnValues.DUPLICATE;
		}

		// Add the quip to the guild
		db.query(`insert into quips(
				guildid,
				authorid,
				quip
			) values(
				$1,
				$2,
				$3
			)`, [
				guildid,
				authorid,
				quip
			]);
		return gb.returnValues.SUCCESS;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Adds a nick to the gulagbot's repertoire
// 	guildid		The ID of the guild in which the nick was added
// 	authorid	The ID of the user who wrote the nick
// 	nick		The nick, verbatim
// Returns:
// 	DUPLICATE, ERROR, SUCCESS
exports.addNick = async function(guildid, authorid, nick) {
	try {
		similarEntries = await db.query('select key from nicks where guildid = $1 and similarity(nick,$2) > $3', [guildid, nick, 0.75]);

		// Check for redflags
		if (similarEntries.rowCount > 0) {
			return gb.returnValues.DUPLICATE;
		}

		// Add the nick to the guild
		db.query(`insert into nicks(
				guildid,
				authorid,
				nick
			) values(
				$1,
				$2,
				$3
			)`, [
				guildid,
				authorid,
				nick
			]);
		return gb.returnValues.SUCCESS;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Log to the console, but only if debugging is enabled
// 	value		The argument to pass straight to console.log
exports.logIfDebug = async function(value) {
	if (gb.config.DEBUG) {
		console.log(value);
	}
}

//
// PUNISHMENTS
//

exports.punishments = {
	IDLE: {
		desc: "IDLE",
		check: async function(decision, status, guildid, id, client) {
			return status <= gb.userStatus.OFFLINE;
		},
		func: async function(client, guild, user) {
			return;
		},
		cost: 0
	},
	DISCONNECT: {
		desc: "DISCONNECT",
		check: async function(decision, status, guildid, id, client) {
			// Note that this is extremely rare at the default cap
			// With a perfectly unlucky roll, you can hit this at 14
			// Otherwise, median score for this is like *27*
			// Alternatively, you can land an 0.01% chance and just get fucked I guess
			return (decision >= 15 || (decision >= 1 && Math.random() > 0.9999)) && status >= gb.userStatus.HUMILIATION;
		},
		func: async function(client, guild, user) {
			for (channelpair of await guild.channels.fetch()) {
				channel = channelpair[1];
				// Skip over any that aren't VCs
				if (!(channel.isVoice())) continue;
				// Find the guy
				for (memberpair of channel.members) {
					member = memberpair[1]
					if (member.user.id == user.id) {
						// We have our guy. DC him
						await member.voice.disconnect("Enacting supreme discipline; this user has been incredibly gulag-worthy as of late, and so we've decided to just disconnect them.");
						gb.punishments.POTSHOT.func(client, guild, user);
					};
				}
			}
		},
		cost: 5
	},
	RENICK: {
		desc: "RENICK",
		check: async function(decision, status, guildid, id, client) {
			guild = await client.guilds.fetch(guildid);
			member = await guild.members.resolve(id);
			// It's no fun if the user has to bug a mod to undo the change, so we only
			// change their nickname if they can change it back themselves
			return (
				decision >= 5 &&
				status >= gb.userStatus.ONLINE &&
				member.moderatable && // Yeah, we can't set mods' nicknames
				guild.me.permissions.has("MANAGE_NICKNAMES") &&
				(
					member.permissions.has("CHANGE_NICKNAME") ||
					member.permissions.has("MANAGE_NICKNAMES")
				)
			);
		},
		func: async function(client, guild, user) {
			member = guild.members.resolve(user.id);
			nick = await db.query('select nick from nicks where guildid = 0::VARCHAR or guildid = $1 order by random() limit 1;', [guild.id]);
			member.setNickname(nick.rows[0].nick, "Enacting discipline; this user will be branded for their crimes");
		},
		cost: 3
	},
	TALKOVERYOU: {
		desc: "TALKOVERYOU",
		check: async function(decision, status, guildid, id, client) {
			return false;
			//return decision >= 3 && status >= gb.userStatus.HUMILIATION;
		},
		func: async function(client, guild, user) {
			return;
		},
		cost: 1
	},
	TEMP_DEAFEN: {
		desc: "TEMP_DEAFEN",
		check: async function(decision, status, guildid, id, client) {
			guild = await client.guilds.fetch(guildid);
			return decision >= 4 + Math.random() * 2 && status >= gb.userStatus.HUMILIATION && guild.me.permissions.has("DEAFEN_MEMBERS");
		},
		func: async function(client, guild, user) {
			for (channelpair of await guild.channels.fetch()) {
				channel = channelpair[1];
				// Skip over any that aren't VCs
				if (!(channel.isVoice())) continue;
				// Find the guy
				for (memberpair of channel.members) {
					member = memberpair[1]
					if (member.user.id == user.id) {
						// We have our guy. Engage trolling
						await member.voice.setDeaf(true, "Enacting discipline; user will hear no capitalist propaganda.");
						member.send("The state thinks you have been hearing too much capitalist propaganda as of late, comrade. Have some glorious Soviet earmuffs.");
						// Sleep 3 seconds
						await new Promise(r => setTimeout(r, 5000));
						// Undeafen the dood
						await member.voice.setDeaf(false, "User has learned their lesson.");
					};
				}
			}
		},
		cost: 5
	},
	TEMP_MUTE: {
		desc: "TEMP_MUTE",
		check: async function(decision, status, guildid, id, client) {
			guild = await client.guilds.fetch(guildid);
			return decision >= 2 + Math.random() * 2 && status >= gb.userStatus.HUMILIATION && guild.me.permissions.has("MUTE_MEMBERS");
		},
		func: async function(client, guild, user) {
			for (channelpair of await guild.channels.fetch()) {
				channel = channelpair[1];
				// Skip over any that aren't VCs
				if (!(channel.isVoice())) continue;
				// Find the guy
				for (memberpair of channel.members) {
					member = memberpair[1]
					if (member.user.id == user.id) {
						// We have our guy. Engage trolling
						await member.voice.setMute(true, "Enacting discipline; user will speak no capitalist propaganda.");
						member.send("Comrade, you are of sayings a lot of fucked up shit. Perhaps you should stop talking for a minute.");
						// Sleep 3 seconds
						await new Promise(r => setTimeout(r, 5000));
						// Undeafen the dood
						await member.voice.setMute(false, "User has learned their lesson.");
					};
				}
			}
		},
		cost: 3
	},
	GULAG: {
		desc: "GULAG",
		check: async function(decision, status, guildid, id, client) {
			guild = await client.guilds.fetch(guildid);
			return decision >= 0.8 + Math.random() * 2 && status >= gb.userStatus.HUMILIATION && guild.me.permissions.has("MOVE_MEMBERS");
		},
		func: async function(client, guild, user) {
			gulags = []
			for (channelpair of await guild.channels.fetch()) {
				channel = channelpair[1];
				if (channel.isVoice() && channel.name.toLowerCase().match(gb.config.GULAG_MATCH_STRING)) {
					gulags.push(channel.id);
				}
			}
			for (channelpair of await guild.channels.fetch()) {
				channel = channelpair[1];
				// Skip over any that aren't VCs
				if (!(channel.isVoice())) continue;
				// Skip over any that are gulags
				if (channel.name.toLowerCase().match(gb.config.GULAG_MATCH_STRING)) continue;
				// Find the guy
				for (memberpair of channel.members) {
					member = memberpair[1]
					if (member.user.id == user.id) {
						// We have our guy. Pick a gulag at random
						targetchannel = Math.floor(Math.random() * gulags.length);
						await member.voice.setChannel(gulags[targetchannel], "Enacting discipline and sending this user to the gulag. They will learn from their transgressions.");
						gb.punishments.POTSHOT.func(client, guild, user);
					};
				}
			}
		},
		cost: 1
	},
	DRIVEBY: {
		desc: "DRIVEBY",
		check: async function(decision, status, guildid, id, client) {
			return false;
			//return decision >= 1 && status >= gb.userStatus.HUMILIATION;
		},
		func: async function(client, guild, user) {
			return;
		},
		cost: 1
	},
	//
	// INTERNAL ACTIONS
	//
	POTSHOT: {
		desc: "POTSHOT",
		check: async function(decision, status, guildid, id, client) {
			return false;
		},
		func: async function(client, guild, user) {
			quip = await db.query('select quip from quips where guildid = 0::VARCHAR or guildid = $1 order by random() limit 1;', [guild.id]);
			user.send(quip.rows[0].quip);
		},
		cost: 0.4
	},
	NONE: {
		desc: "NONE",
		check: async function(decision, status, guildid, id, client) {
			return true;
		},
		func: async function(client, guild, user) {
			return;
		},
		cost: 0.0001
	}
}

// Have the gulagbot decide on a punishment for a user based on a variety of
// factors.
// 	guildid		The guildid of the user
// 	id		The ID of the user
// 	maxscore	The amount of score the gulagbot is willing to expend
// 	status		The status of the user as determined by exports.getUserStatus
// 	client		A discord.js client
// Returns:
// 	An element from exports.punishments
// 	ERROR
exports.decidePunishment = async function(guildid, id, maxscore, status, client) {
	// This weighting should make smaller punishments more common at smaller
	// scores. Higher scores get worse punishments.
	decision = parseFloat(Math.random() * maxscore + Math.log(maxscore)).toFixed(2);
	gb.logIfDebug('Deciding punishment for ' + guildid + ':' + id + ', $' + maxscore + ', status ' + status + ', decision ' + decision);
	try {
		for (p in gb.punishments) {
			punishment = gb.punishments[p];
			check = await punishment.check(decision, status, guildid, id, client);
			if (check == true) {
				return punishment;
			}
		}
		return gb.punishments.NONE;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

exports.userStatus = {
	HUMILIATION: 10,
	VOICE: 5,
	ONLINE: 1,
	OFFLINE: 0
}

// Get the status of a user as it pertains to making an enforcement decision.
// 	client		A discord.js client
// 	guildid		The guildid of the user
// 	id		The ID of the user
// Returns:
// 	A child of the above structure:
// 	"HUMILIATION"	The user is online, in a voice chat, and meets the
// 			requirements for humiliation
// 	"VOICE"		The user is online, in a voice chat, and not among friends
// 	"ONLINE"	The user is online, but not in a voice chat
// 	"OFFLINE"	The user is offline/away
// 	ERROR
exports.getUserStatus = async function(client, guildid, id) {
	try {
		gb.logIfDebug('Getting status for user ' + guildid + ':' + id);
		guild = await client.guilds.fetch(guildid);
		user = await guild.members.fetch(id);

		// Is the user online at all?
		if (!user || !user.presence || !user.presence.status || user.presence.status == "offline" || user.presence.status == "idle" || user.presence.status == "dnd" ) {
			gb.logIfDebug('OFFLINE');
			return gb.userStatus.OFFLINE;
		}
		// Is the user in a voice chat?
		for (channelpair of await guild.channels.fetch()) {
			channel = channelpair[1];
			// Skip over any that aren't VCs
			if (!(channel.isVoice())) continue;
			// Skip over any that are gulags
			if (channel.name.toLowerCase().match(gb.config.GULAG_MATCH_STRING)) continue;
			// See if we meet the humiliation threshold
			usercount = 0
			foundUser = false
			for (memberpair of channel.members) {
				if (!memberpair[1].user.bot) usercount++;
				if (memberpair[1].user.id == id) foundUser = true;
			}
			if (!foundUser) continue;
			if (usercount >= gb.config.HUMILIATION) {
				gb.logIfDebug('HUMILIATION');
				return gb.userStatus.HUMILIATION;
			} else {
				gb.logIfDebug('VOICE');
				return gb.userStatus.VOICE;
			}
		}
		// We failed out of the VC path but not the offline path; we must be online
		gb.logIfDebug('ONLINE');
		return gb.userStatus.ONLINE;
	} catch(e) {
		console.error(e);
		return gb.returnValues.ERROR;
	}
}

// Have the gulagbot iterate through guilds and users and try to enforce
// discipline
// 	client		A discord.js client
// Returns:
// 	ERROR, SUCCESS
exports.enforce = async function(client) {
	try {
		// Get a list of all users who need discipline
		query = await db.query('select guildid, id, score from users where score > 0');
		// If there are none, bail
		if (query.rowCount < 1) return gb.returnValues.SUCCESS;
		gb.logIfDebug('Beginning enforcement run...');

		// Iterate over all bad actors, looking for ones we can punish
		for (row of query.rows) {
			try {
				// User details are used for updating the users table
				guild = await client.guilds.fetch(row.guildid);
				if (!guild) continue;
				user = await guild.members.fetch(row.id);
				if (!user) continue;
				gb.logIfDebug('Enforcing for ' + row.guildid + ':' + row.id + ', ' + row.score + ' (' + user.displayName + ')');
				// These values are actually used for enforcement
				status = await gb.getUserStatus(client, row.guildid, row.id);
				punishment = await gb.decidePunishment(row.guildid, row.id, row.score, status, client);
				gb.logIfDebug('Picked "' + punishment.desc + '" with cost ' + punishment.cost);
				// Do the needful
				punishment.func(client, guild, user)
					.catch((e) => {
						console.error("Error enforcing punishment for " + row.guildid + ':' + row.id + ' (' + user.displayName + ')');
						console.error(e);
					});
				// Mark the user down as affected
				await gb.affectUser(row.guildid, client.user.id, row.id, user.displayName, 0, -1 * punishment.cost, punishment.desc);
			} catch(e) {
				// Mask permission denied errors
				if (!e.code || e.code != 50001) {
					console.error(e);
					return gb.returnValues.ERROR;
				}
			}
		}
		return gb.returnValues.SUCCESS;
	} catch(e) {
		console.error(e);
	}
}
