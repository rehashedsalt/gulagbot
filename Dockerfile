FROM node:16-alpine
COPY ./ /home/node/app/
WORKDIR /home/node/app/
RUN npm install
CMD npm start
