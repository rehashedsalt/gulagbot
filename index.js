/*
 * index.js
 * Example frontend for gulagbot
 * Copyright (C) 2021 Vintage Salt
 *
 * Distributed under terms of the MIT license.
 */

require('log-timestamp');

const Discord = require('discord.js');
const pg = require('pg');
const gulag = require('./gulag.js');
const defaultQuips = require('./quips.json');
const defaultNicks = require('./nicks.json');
const debugUserID = "198274637006635008";

// Connect to Discord
const client = new Discord.Client({
	intents: [ 
		Discord.Intents.FLAGS.DIRECT_MESSAGES,
		Discord.Intents.FLAGS.GUILDS,
		Discord.Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
		Discord.Intents.FLAGS.GUILD_MEMBERS,
		Discord.Intents.FLAGS.GUILD_MESSAGES,
		Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
		Discord.Intents.FLAGS.GUILD_PRESENCES,
		Discord.Intents.FLAGS.GUILD_VOICE_STATES
	]
});
client.once('ready', async () => {
	console.log('Discord connection established as ' + client.user.username);
	if (process.env.GULAG_DEBUG == 'false') {
		gulag.config.DEBUG = false;
	} else if (process.env.GULAG_DEBUG) {
		gulag.config.DEBUG = true;
	} else {
		gulag.config.DEBUG = false;
	}
	gulag.config.ENFORCE_FREQUENCY = parseInt(process.env.GULAG_ENFORCE_FREQUENCY) || 30 * 1000;
	gulag.config.HUMILIATION = parseInt(process.env.GULAG_HUMILIATION);
	gulag.config.SCORE_MAX = parseInt(process.env.GULAG_SCORE_MAX) || 50;
	gulag.config.SCORE_MIN = parseInt(process.env.GULAG_SCORE_MIN) || -3;
	gulag.config.ID = client.user.id;
	gulag.logIfDebug(gulag.config);

	// Add default gulag quips
	console.log('Adding default quips...');
	for (key in defaultQuips) {
		gulag.addQuip(0, client.user.id, defaultQuips[key]);
	}

	// Add default nicks
	console.log('Adding default nicks...');
	for (key in defaultNicks) {
		gulag.addNick(0, client.user.id, defaultNicks[key]);
	}

	// Add slash commands
	console.log('Adding/updating slash commands...');
	commands = [
		{
			name: "gulag",
			description: "Gulag a user",
			options: [
				{
					name: "user",
					type: 'USER',
					description: "The user to gulag",
					required: true
				},
				{
					name: "reason",
					type: 'STRING',
					description: "The reason why this user is getting gulagged",
					required: false
				}
			]
		},
		{
			name: "praise",
			description: "Praise a user",
			options: [
				{
					name: "user",
					type: 'USER',
					description: "The user to praise",
					required: true
				},
				{
					name: "reason",
					type: 'STRING',
					description: "The reason why this user is getting praised",
					required: false
				}
			]
		},
		{
			name: "addquip",
			description: "Add a snarky response to the Gulag Bot for this server",
			options: [
				{
					name: "quip",
					type: 'STRING',
					description: "The quip",
					required: true
				}
			]
		},
		{
			name: "addnick",
			description: "Add a nickname for the Gulag Bot to use on this server",
			options: [
				{
					name: "nick",
					type: 'STRING',
					description: "The nickname",
					required: true
				}
			]
		},
		{
			name: "checkgulag",
			description: "Check this server to see which users have committed wrongthink",
			options: [
				{
					name: "user",
					type: 'USER',
					description: "An optional user to check the score of directly",
					required: false
				}
			]
		}
	];
	//client.application.commands.set(commands);
	client.application.commands.set([])
		.catch((e) => {
			console.error("Unable to clear application commands");
		});
	for (guildpair of await client.guilds.fetch()) {
		guild = await guildpair[1].fetch();
		await guild.commands.set(commands)
			.catch((e) => {
				console.error("Unable to set guild commands for " + guild.id);
				console.error(e);
			});
	}

	// Set up our gulag timer thing
	setInterval(() => gulag.enforce(client), gulag.config.ENFORCE_FREQUENCY);
	console.log('Post-ready initialization complete. Ready to go.');
});
console.log('Connecting to Discord...');
client.login(process.env.DISCORD_TOKEN);

// Handle commands
client.on('interactionCreate', async (interaction) => {
	try {
		if (!interaction.isCommand() || !interaction.guildId) return;
		returnmsg = "Stalin is unsure of what happened, comrade. Please report this."
		switch(interaction.commandName) {
			case "gulag":
				user = interaction.options.get("user", true);
				note = interaction.options.get("reason") || { value: null };
				switch (await gulag.affectUser(interaction.guildId, interaction.member.id, user.user.id, user.user.username, interaction.id, 1, note.value)) {
					case gulag.returnValues.CAP:
						returnmsg="Stalin only has so much capacity for hate, comrade. Leave this user be.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.DUPLICATE:
						returnmsg="Stalin not sure how, but he thinks he has seen this message before.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.ERROR:
						returnmsg="KGB is busy, comrade. Try again later.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.GUILTY:
						returnmsg="You expect Stalin to listen to *you* of all people? Ha! Is laughable prospect. I laugh. Fuck you.";
						break;
					case gulag.returnValues.SAMEUSER:
						returnmsg="Comrade, Stalin refuses to let you do this to yourself. Lay off the vodka for tonight.";
						break;
					case gulag.returnValues.GULAGBOT:
						returnmsg="Comrade. Come on.";
						break;
					case gulag.returnValues.SUCCESS:
						returnmsg="Stalin has special place in gulag for " + user.user.username + ".";
						break;
				}
				break;
			case "praise":
				user = interaction.options.get("user", true);
				note = interaction.options.get("reason") || { value: null };
				switch (await gulag.affectUser(interaction.guildId, interaction.member.id, user.user.id, user.user.username, interaction.id, -1, note.value)) {
					case gulag.returnValues.CAP:
						returnmsg="This user is already pinnacle of communism; no praise make them any better, comrade.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.DUPLICATE:
						returnmsg="Stalin not sure how, but he thinks he has seen this message before.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.ERROR:
						returnmsg="KGB is busy, comrade. Try again later.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.GUILTY:
						returnmsg="*You* think this user is of deservings praise? Means little.";
						break;
					case gulag.returnValues.SAMEUSER:
						returnmsg="Stalin does not appreciate comrades being full of themselves.";
						break;
					case gulag.returnValues.GULAGBOT:
						returnmsg="Stalin appreciates it, comrade, but no.";
						break;
					case gulag.returnValues.SUCCESS:
						returnmsg="Stalin will ensure that " + user.user.username + " gets extra rations.";
						break;
				}
				break;
			case "addquip":
				quip = interaction.options.get("quip", true);
				switch(await gulag.addQuip(interaction.guildId, interaction.member.id, quip.value)) {
					case gulag.returnValues.DUPLICATE:
						returnmsg="Stalin thinks he has heard that one before, comrade. Dig deep, be of originality.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.ERROR:
						returnmsg="Say again? Stalin not quite catch you that time, comrade.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.SUCCESS:
						returnmsg="Hah! Good one, comrade! Stalin will keep that in mind for next time.";
						break;
				}
				break;
			case "addnick":
				nick = interaction.options.get("nick", true);
				switch(await gulag.addNick(interaction.guildId, interaction.member.id, nick.value)) {
					case gulag.returnValues.DUPLICATE:
						returnmsg="That one is already in Stalin's list, comrade.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.ERROR:
						returnmsg="Sorry, say again? Stalin was not listening.";
						returnmsg = { content: returnmsg, ephemeral: true };
						break;
					case gulag.returnValues.SUCCESS:
						returnmsg="Fantastic idea, comrade! Stalin will use this in the future.";
						break;
				}
				break;
			case "checkgulag":
				user = interaction.options.get("user");
				if (user) {
					data = parseFloat(await gulag.getUserScore(interaction.guildId, user.user.id)).toFixed(2);
					returnmsg = user.user.username + ": " + data;
					returnmsg = { content: returnmsg, ephemeral: true };
				} else {
					data = await gulag.getScoreboard(interaction.guildId);
					if (data.rowCount > 0) {
						returnmsg="As you wish. Here is hitlist for this server, comrade:";
						for (row of data.rows) {
							if (row.score > 0.5) {
								returnmsg +="\n**☐** - " + row.name + " - " + row.score;
							} else if (row.score < 0) {
								returnmsg +="\n**☐** - " + row.name + " - " + -1 * row.score + " praise";
							}
						}
					} else {
						returnmsg="Hitlist for this server is clean, comrade. No wrongthinkers.";
					}
					returnmsg = { content: returnmsg, ephemeral: true };
				}
				break;
			default:
				console.error("Received unknown command " + interaction.commandName);
				returnmsg="Stalin does not know this command; please report this upstream, comrade.";
				returnmsg = { content: returnmsg, ephemeral: true };
				break;
		}
		interaction.reply(returnmsg);
	}
	catch(e) {
		console.error(e);
	}
});

// Handle reacts
client.on('messageReactionAdd', async (messageReaction, user) => {
	try {
		msg = messageReaction.message;
		switch(messageReaction.emoji.name) {
			case "gulag":
				switch(await gulag.affectUser(msg.guildId, user.id, msg.author.id, msg.author.username, msg.id, 1)) {
					case gulag.returnValues.CAP:
						msg.react('🚫');
						break;
					case gulag.returnValues.DUPLICATE:
						break;
					case gulag.returnValues.GUILTY:
						msg.react('💀');
						break;
					case gulag.returnValues.GULAGBOT:
						msg.react('😂');
						break;
					case gulag.returnValues.SAMEUSER:
						msg.react('❓');
						break;
					case gulag.returnValues.SUCCESS:
						gulagReact = msg.guild.emojis.cache.find(emoji => emoji.name == 'stalinangry');
						if (gulagReact) {
							msg.react(gulagReact.id);
						} else {
							msg.react('⚒️');
						}
						break;
					default:
						msg.react('⌛');
						break;
				}
				break;
			case "praise":
				switch(await gulag.affectUser(msg.guildId, user.id, msg.author.id, msg.author.username, msg.id, -1)) {
					case gulag.returnValues.CAP:
						msg.react('⭐');
						break;
					case gulag.returnValues.DUPLICATE:
						break;
					case gulag.returnValues.GUILTY:
						msg.react('💀');
						break;
					case gulag.returnValues.GULAGBOT:
						msg.react('😂');
						break;
					case gulag.returnValues.SAMEUSER:
						msg.react('❓');
						break;
					case gulag.returnValues.SUCCESS:
						praiseReact = msg.guild.emojis.cache.find(emoji => emoji.name == 'stalinhappy');
						if (praiseReact) {
							msg.react(praiseReact.id);
						} else {
							msg.react('⚒️');
						}
						break;
					default:
						msg.react('⌛');
						break;
				}
				break;
		}
	} catch(e) {
		console.error(e);
	}
});

// Handle message commands
client.on('messageCreate', async (message) => {
	try {
		// Ignore anything that's not a command
		prefix = gulag.config.COMMAND_PREFIX;
		if (!message.content.startsWith(prefix)) return;
		// Ignore all DMs from users who aren't the debug user
		if (message.guild == null && message.author.id != debugUserID) return;
		const args = message.content.slice(prefix.length).trim().split(' ');
		const command = args.shift().toLowerCase();
		const msg = message.content.slice(prefix.length + command.length).trim();
		returnmsg = "Test message please ignore";
		// Get a list of all users mentioned; we're gonna need it for later
		mentionedUsers = []
		for (pair of message.mentions.users) {
			mentionedUsers.push(pair[1]);
		}
		if (message.mentions.roles) {
			message.guild.members.fetch();
		}
		for (pair of message.mentions.roles) {
			gulag.logIfDebug('Looking at role ' + pair[1].name);
			for (userpair of pair[1].members) {
				gulag.logIfDebug('Adding member of role: ' + userpair[1].user.username);
				mentionedUsers.push(userpair[1].user);
			}
		}
		gulag.logIfDebug(mentionedUsers);
		switch(command) {
			case "stalin":
				returnmsg="Da. Say word and wrongthinkers will disappear, comrade.";
				if (message.author.id == debugUserID && gulag.config.DEBUG) {
					switch(args[0]) {
						case "logdump":
							log = await gulag.getPrettyLog(message.guildId);
							logfile = new Discord.MessageAttachment(Buffer.from(log.join('\n')), "gulagbot-log.txt");
							message.author.send({ content: "Here is log, comrade.", files: [ logfile ] });
							returnmsg="Da. On its way in full, comrade.";
							break;
						default:
							returnmsg="Stalin not understand request, comrade.";
							break;
					}
				}
				break;
			case "addquip":
				returnmsg="Consider using `/addquip` instead, comrade.";
				break;
			case "gulag":
				returnmsg="Consider using `/gulag` instead, comrade. KGB worked very hard on it.";
				break;
			case "praise":
				returnmsg="Use `/praise`, comrade; is easier for Stalin to track.";
				break;
			case "checkgulag":
				returnmsg="Use `/checkgulag`, comrade; less noise that way.";
				break;
			default:
				return;
		}
		message.channel.send(returnmsg)
			.catch(e => {
				console.error("Failed to send response: " + message.author.username + "/" + message.author.id + ": " + message.content);
				console.error(e);
			});
	} catch(e) {
		console.error(e);
	}
});
